# redtk.redtk-extensions-common

## Installation

Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter.

```text
ext install redtk.redtk-extensions-common
```

## Prepared for
VS Code common use cases.
